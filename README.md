my api backend  https://radiant-bayou-53976.herokuapp.com/

# Cara hosting backend nodejs mongodb di heroku
1. pertama kita harus masuk terlebihdahulu ke heroku register terlebihdahulu
2. setelah itu kita minkan git saja di terminal / cmd 
3. untuk perintahnya kita harus masuk ke directory backend terlebih dahulu 
 setelah masuk ke directory backend kita coba masukan perintah <b>git init</b>
 git init untuk membuat system git di folder kita untuk meremote ke heroku langsung
 setelah itu kita jangan lupa harus intall dulu heroku cli nya dengan perintah 
 untuk mengenai intal heroku cli boleh visit <a href="https://devcenter.heroku.com/articles/heroku-cli">heroku cli</a>

 step satu jangan lupa masuk directory foldernya
 ```
 git init

 --ini untuk install git di folder kita
 ```
kedua klw belum install heroku alangkah baik install dulu dengan perintah
```
--untuk ubuntu

sudo snap install --classic heroku

--untuk windows bisa chek ke heroku.com

```
setelah itu kita login ke heroku
```
heroku login
```
setelah login kita bisa buat folder di heroku
```
heroku create
```

setelah itu jangan lupa buat procfile yaitu extension heroku sendiri
```
---perintah di terminal

touch Procfile

dan tambah di dalamnya kode start projectmu misal jikalau menggunakan nodejs 
saya suka memakai perintah start dengan ,, -- npm start -- dengan type

web:npm start

bisa di liat di package.json didalamnya ada start
```

 dan jangan lupa mengenai database harus sesuai dengan heroku inginkan, 
 misal kalau menggunakan mongodb kita harus pake link mongodb contoh, 

 mongodb+srv://yourdbname:<passwordanda>cluster0.ma8no.mongodb.net/namadataanda

 link di bawah di atas harus register terlebihdahulu
 
  langsung dan atur lagi process.env.PORT nya harus sesuai heroku ,
  untuk contoh punya saya seperti di bawah ini

 contoh 
```
    let port = process.env.PORT;
    if(port == null || port == ''){
    port = 5000

    }
    app.listen(port, () => {
    console.log(`App runs on http://localhost:${port}`);
    }); 
```

nah setelah itu di buat kita masukan engines di package.json kita 
disini saya memakai node.js , jadi kita harus masukan type node.js nya agar 
si heroku tau , unutk mengetahui version node kita pake,, node --version lalu masukan versionnya sprti 
yang saya contohkan

dan jangan lupa sisipkan kode di bawah ini ke package.json 
```
 "engines":{
    "node":"12.19.0"
  },

  ```
  exactly menyimpan engines di bawah 
  ``` 
  "license":"ISC" 
  ```

  setelah semua itu sudah di buat dan sebelum kita mendeploy project kita
  kita harus buat dulu .gitignore agar saat push kita tidak membawa node_modules
  untuk contoh di dalam .gitignore
 ```
 /node_modules
 npm debug.log
 ```
 setelah itu kita siap deh untuk mendeploy project kita dengan perintah git sederhana

 ```
 untuk add project kita pake 

 git add .

 setelah kita kasih commit

 git commit -m "keterangan anda"

 setelah itu kita push deh

 git push heroku master